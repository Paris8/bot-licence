import discord
from discord.ext import commands

from random import randint
import lyricsgenius
import time
from tokens import token_genius as token # à l'importation de l'extension, le fichier se retrouve dans le '/' et non dans 'cogs/', ignorez l'erreur pylint si il y a
genius = lyricsgenius.Genius(token)

def setup(bot):
    bot.add_cog(Lyrics(bot))

class Lyrics(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='lyrics', aliases = ['l', 'lyric'])
    async def _lyrics(self, ctx, *, song: str = None):
        """Affiche les paroles de la musique en cours, ou de la chanson spécifiée.\n	➡ Syntaxe: .lyrics/lyric/l (musique)⁢⁢⁢⁢⁢⁢⁢⁢⁢⁢"""
        if song:
            if " romanized" in song:
                message = await ctx.send(f":mag: **Cherche les paroles romanisées de ** `{song.replace(' romanized', '')}`")
            else:
                message = await ctx.send(f":mag: **Cherche les paroles de ** `{song}`")
            temps_requete = int(round(time.time() * 1000))
            song_genius = genius.search_song(song)
            couleur_embed = randint(0, 0xFFFFFF)
            try:
                paroles = song_genius.lyrics
            except:
                await ctx.message.add_reaction(emoji = '❌')
                return await message.edit(content = f"Pas de résultats trouvés pour `{song}`.") 
            lignetotal = ""
            premierembed = True
            if len(paroles) > 7500:
                await ctx.message.add_reaction(emoji = '❌')
                return await message.edit(content = f"Désolé, les paroles sont trop longues pour être affichés (lien vers la page des paroles : {song_genius.url}).")
            title_first_embed = f"Paroles de {song_genius.title} par {song_genius.artist}."
            desc_first_embed = f"[Lien vers les paroles sur le site]({song_genius.url})"
            type_de_comptage = "\n\n" if paroles.count("\n\n") > 2 else "\n"
            for ligne in paroles.split(type_de_comptage):
                if len(ligne) >= 2048:
                    type_de_comptage = "\n"
            for ligne in paroles.split(type_de_comptage):
                if len(f"{lignetotal}{type_de_comptage}{ligne}") < 1900:
                    lignetotal = f"{lignetotal}{type_de_comptage}{self.ligne_formatage(ligne)}"
                else:
                    if premierembed == True:
                        premierembed = False
                        embed = discord.Embed(title = title_first_embed, description = f"{desc_first_embed}{lignetotal}", color = couleur_embed)
                        embed.set_thumbnail(url = song_genius.song_art_image_url)
                        await message.edit(embed = embed)
                    else:
                        embed = discord.Embed(description = lignetotal, color = couleur_embed)
                        await ctx.send(embed = embed)
                    lignetotal = f"{self.ligne_formatage(ligne)}"
            
            temps_requete = int(round(time.time() * 1000)) - temps_requete
            footer_embed = f"Pour {self.user_or_nick(ctx.author)} par Genius en {round(temps_requete / 1000, 2)} s."
            await ctx.message.add_reaction(emoji = '✅')
            if premierembed == True:
                premierembed = False
                embed = discord.Embed(title = title_first_embed, description = f"{desc_first_embed}{lignetotal}", color = couleur_embed)
                embed.set_footer(icon_url = ctx.author.avatar_url, text = footer_embed)
                return await message.edit(embed = embed)
            else:
                embed = discord.Embed(description = lignetotal, color = couleur_embed)
                embed.set_footer(icon_url = ctx.author.avatar_url, text = footer_embed)
                return await ctx.send(embed = embed)
        else:
            await ctx.message.add_reaction(emoji = '❌')
            await ctx.send("Aucune musique demandé... `.lyrics/l/lyrics <song>`.")
    def ligne_formatage(self, ligne):
        liste_balise = [
            ('[Hook', '[Accroche'), ('[Verse', '[Couplet'), ('[Chorus', '[Chœur'),
            ('[Bridge', '[Pont'),('[Pre-Chorus', '[Pré-chœur'), ('[Post-Chorus', '[Post-chœur')
        ]
        for balises in liste_balise:
            ligne = ligne.replace(balises[0], balises[1])
        return ligne
    def user_or_nick(self, user):
        if user.nick:
            return f"{user.nick} ({user.name}#{user.discriminator})"
        else:
            return f"{user.name}#{user.discriminator}"

    @commands.command(name='lyricsromanized', aliases = ['lr', 'lyricromanized'], hidden = True)
    async def _lyricsromanized(self, ctx, *, song: str = None):
        await ctx.invoke(self.bot.get_command("lyrics"), song = f"{song} romanized" if song else song)
