# Bot pour le cours de création et gestion de ressources

#### __Pré-requis__

- Python 3.8.6
- [requirements.txt](requirements.txt)

#### __Mise en place__

- Un fichier `tokens.py` qui contient :

```python
token_discord = "ton token discord"
token_genius = "ton token genius"
token_reddit = {"client_id": "ton client_id", "client_secret": "ton client_secret", "user_agent": "ton pseudo reddit"}
```
Pour trouver ton token reddit, va sur [ce site](https://www.reddit.com/prefs/apps) et voici les instructions : ![instructions](https://i.imgur.com/W5La6v6.png)

*rediriger uri (pour le copié/collé) : http://localhost:8080*

#### __Mise en marche__

- Lancer [main.py](main.py)

#### __Ajout du bot à votre serveur__

- [Ce site](https://discordapi.com/permissions.html) vous permet de choisir quelles permissions ajouter par défaut au bot.
    - Choissisez *Administrator* pour pas se prendre la tête.
    - Copier coller l'ID de votre bot dans *Client ID* trouvable [ici](https://discord.com/developers/applications) et rendez-vous sur le lien en bas de page.

## __Fonctionnalités__

| Nom  | Fonction |
| ---:| :--- |
| [Réactions messages](https://code.up8.edu/Anri/bot-licence/-/blob/master/main.py#L27) | Quand on réagis à un message spécifique avec certains emojis, ça te donne le rôle correspondant |
| [Citation](https://code.up8.edu/Anri/bot-licence/-/blob/master/main.py#L95) | En copiant le lien d'un message, donne une mise en forme |
| [.calc](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/commands.py#L40) | Fais un calcul simple |
| [.help](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/help.py#L16) | Renvois la liste des commandes disponible |
| [.syntax](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/commands.py#L82) | Explique la syntaxe des messages sur Discord |
| [.ping](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/commands.py#L22) | Vérifie le ping et la latence du bot |
| [.info](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/commands.py#L128) | Pour obtenir des infos sur le bot |
| [.whois](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/commands.py#L160) | Pour obtenir des infos sur un membre |
| [.appel](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/commands.py#L220) | Pour faire l'appel facilement |
| [.sondage](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/commands.py#L253) | Pour faire un sondage facilement |
| [.avatar](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/commands.py#L317) | Affiche l'avatar de quelqu'un |
| [.lyrics](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/lyrics.py#L19) | Cherche des paroles de chanson sur Genius |
| [.memes](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/internet.py#L17) | Envoie un meme depuis reddit |
| [.cat](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/internet.py#74) | Envoie un chat depuis internet |
| [.dog](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/internet.py#L90) | Envoie un chien depuis internet |
| [.chifumi](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/games.py#L17) | Fais une partie de pierre papier ciseaux contre le bot |
| [.plusoumoins](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/games.py#L49) | Fais une partie de guessing game contre le bot |
| [.news](https://code.up8.edu/Anri/bot-licence/-/blob/master/cogs/internet.py#L105) | Info random dans le domaine de l'informatique |
