print("Lancement du bot de la licence")

import discord, re
from discord.ext import commands
from tokens import token_discord as token
from pytz import timezone

# on décide du prefix
# on dit d'ignorer les majuscules/minuscules quand on tape une commande
# plus d'infos sur l'intent ici : https://discordpy.readthedocs.io/en/latest/intents.html (j'ai pas encore tout compris c'est nouveau)
client = commands.Bot(command_prefix = ".", case_insensitive = True, intents = discord.Intents.all())

@client.event
async def on_connect():
    print(f"Connecté avec le token : {token}.")
    print("Chargement des extensions & librairie...")
    client.load_extension("cogs.commands")
    client.load_extension("cogs.help")
    client.load_extension("cogs.games")
    client.load_extension("cogs.internet")
    client.load_extension("cogs.lyrics")

# le status du bot
@client.event
async def on_ready():
    await client.change_presence(status = discord.Status.online, activity = discord.Activity(name = "https://code.up8.edu/Anri/bot-licence",
                                 type = discord.ActivityType.competing))
    print("Bot prêt.")

@client.event
async def on_raw_reaction_add(payload): # check toutes les nouvelles réactions
    if payload.message_id == 765470422149365760: # on check si on réagis au message que l'on veut
        # en fonction de la réaction on donne le role
        if payload.emoji.name == '🟡':
            role = discord.utils.get(payload.member.guild.roles, name="L1A1")
            await payload.member.add_roles(role)
        if payload.emoji.name == '🟠':
            role = discord.utils.get(payload.member.guild.roles, name="L1A2")
            await payload.member.add_roles(role)
        if payload.emoji.name == '🔵':
            role = discord.utils.get(payload.member.guild.roles, name="L1B1")
            await payload.member.add_roles(role)
        if payload.emoji.name == '🟣':
            role = discord.utils.get(payload.member.guild.roles, name="L1B2")
            await payload.member.add_roles(role)
        if payload.emoji.name == '🟤':
            role = discord.utils.get(payload.member.guild.roles, name="L1C1")
            await payload.member.add_roles(role)
        if payload.emoji.name == '🟢':
            role = discord.utils.get(payload.member.guild.roles, name="L1C2")
            await payload.member.add_roles(role)

# meme systeme qu'au dessus sauf que c'est plus compliqué de choppé le membre pour lui retirer son rôle
@client.event
async def on_raw_reaction_remove(payload):
    if payload.message_id == 765470422149365760:
        if payload.emoji.name == '🟡':
            guild = discord.utils.find(lambda g : g.id == payload.guild_id, client.guilds)
            member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
            role = discord.utils.get(guild.roles, name="L1A1")
            await member.remove_roles(role)
        if payload.emoji.name == '🟠':
            guild = discord.utils.find(lambda g : g.id == payload.guild_id, client.guilds)
            member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
            role = discord.utils.get(guild.roles, name="L1A2")
            await member.remove_roles(role)
        if payload.emoji.name == '🔵':
            guild = discord.utils.find(lambda g : g.id == payload.guild_id, client.guilds)
            member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
            role = discord.utils.get(guild.roles, name="L1B1")
            await member.remove_roles(role)
        if payload.emoji.name == '🟣':
            guild = discord.utils.find(lambda g : g.id == payload.guild_id, client.guilds)
            member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
            role = discord.utils.get(guild.roles, name="L1B2")
            await member.remove_roles(role)
        if payload.emoji.name == '🟤':
            guild = discord.utils.find(lambda g : g.id == payload.guild_id, client.guilds)
            member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
            role = discord.utils.get(guild.roles, name="L1C1")
            await member.remove_roles(role)
        if payload.emoji.name == '🟢':
            guild = discord.utils.find(lambda g : g.id == payload.guild_id, client.guilds)
            member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
            role = discord.utils.get(guild.roles, name="L1C2")
            await member.remove_roles(role)

@client.event
async def on_command_error(ctx, error):
    if not ctx.invoked_with.startswith('.'):
        print(error)
        await ctx.message.add_reaction(emoji = '❓')

@client.event
async def on_message(message):
    await client.process_commands(message)

    if message.author == client.user:
        return
    
    if client.user.mention == message.content.replace("!",""):
        ctx = await client.get_context(message)
        prefix = await client.get_prefix(message)
        await ctx.send(f">>> Coucou !\nMon préfix est `{prefix}` et ma commande d'aide est `{prefix}help`")
    
    urls = re.findall(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', message.content)
    for i in range(len(urls)):
        if urls[i].startswith("https://discordapp.com/channels/") or urls[i].startswith("https://discord.com/channels/") or urls[i].startswith("https://ptb.discordapp.com/"):
            link = urls[i]
            linkURL = link
            if link.startswith("https://discord.com/channels/"):
                link = f'000{link}'
            if link.startswith("https://ptb.discordapp.com/"):
                link = link[4:]
            if "@me" in urls[i]:
                return await message.channel.send("Je ne cite pas les messages privés.", delete_after = 5)
            try:
                if int(link[32:-38]) == message.guild.id:
                    msgID = await client.get_channel(int(link[51:-19])).fetch_message(int(link[70:]))
                    couleur = 0x2f3136
                    if len(msgID.content) > 0:
                        embed = discord.Embed(description = msgID.content, colour = couleur)
                    else:
                        embed = discord.Embed(description = "Pas de message (image ? vidéo ? intégration ?)", colour = couleur) # peut etre implémenter autre chose
                    auteur = "Auteur"
                    if message.author == msgID.author:
                        auteur = "Auteur & Citateur"
                    embed.add_field(name = auteur, value = msgID.author.mention, inline=True)
                    embed.add_field(name = "Channel", value = msgID.channel.mention, inline=True)
                    embed.add_field(name = "Message", value = f"[Aller au message]({linkURL})", inline=True)
                    embed.set_author(name = "Citation", icon_url = msgID.author.avatar_url)

                    icon_url = message.author.avatar_url

                    date_1 = str(msgID.created_at.astimezone(timezone('Europe/Paris')))[:-13].replace('-', '/').split()
                    edit = ""
                    if msgID.edited_at:
                        date_edit = str(msgID.edited_at.astimezone(timezone('Europe/Paris')))[:-13].replace('-', '/').split()
                        edit = f"(Dernier edit : {date_edit[0][8:]}/{date_edit[0][5:-3]}/{date_edit[0][:4]} à {date_edit[1]})"
                    message_1 = f"Date : {date_1[0][8:]}/{date_1[0][5:-3]}/{date_1[0][:4]} à {date_1[1]} {edit}"

                    date_2 = str(message.created_at.astimezone(timezone('Europe/Paris')))[:-13].replace('-', '/').split()
                    date_2 = f"{date_2[0][8:]}/{date_2[0][5:-3]}/{date_2[0][:4]} à {date_2[1]}"

                    cite = ""
                    if auteur == "Auteur":
                        cite = f"\nCité par {user_or_nick(message.author)} le {date_2}"
                    embed.set_footer(icon_url = icon_url, text = f"{message_1}{cite}")
                    await message.channel.send(embed = embed)
                    if message.content == linkURL.replace(' ',''):
                        await message.delete()
            except Exception as e:
                e = str(e)
                if not "invalid literal for int() with base 10:" in e or not "404 Not Found (error code: 10008)" in e: # faute de frappe / message supprimé
                    print(e)
def user_or_nick(user):
    if user.nick:
        return f"{user.nick} ({user.name}#{user.discriminator})"
    else:
        return f"{user.name}#{user.discriminator}"

# pour lancer le bot
client.run(token)
